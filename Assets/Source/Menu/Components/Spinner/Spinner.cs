﻿namespace Developer.Component
{
    using UnityEngine;

    /// <inheritdoc />
    /// <summary>
    /// Simple spinner to tell the user that something is loading.
    /// </summary>
    public class Spinner : MonoBehaviour
    {
        private const float speed = 3.0f;

        private void Update()
        {
            gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x, gameObject.transform.eulerAngles.y, gameObject.transform.eulerAngles.z - speed);
        }
    }
}