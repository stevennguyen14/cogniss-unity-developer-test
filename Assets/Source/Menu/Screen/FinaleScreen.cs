﻿//TODO: Complete this screen
namespace Developer.Screen
{
	using UnityEngine;
	using UnityEngine.UI;
	using UnityEngine.SceneManagement;
	using System.Reflection;

	public sealed class FinaleScreen : Core.ScreenAbstract
	{
		[SerializeField]
		private GameObject buttonObject;
		[SerializeField]
		private Camera camera;
		private Button button;

		private void Awake()
		{
			//dynamically link up the button and set it active
			//find child object "Button"
			buttonObject = gameObject.transform.Find("Button").gameObject;
			buttonObject.gameObject.SetActive(true);

			//set sibling index to first so players can see the button
			transform.SetSiblingIndex(0);

			//set the button listener
			button = buttonObject.GetComponent<Button>();
			button.onClick.AddListener(Restart);

			//assign background color accordingly
			camera = Camera.main.gameObject.GetComponent<Camera>();

			//ANDROID IS RED
			if (Application.platform == RuntimePlatform.Android)
			{
				camera.backgroundColor = Color.red;
			}
			//IOS IS GREEN
			else if (Application.platform == RuntimePlatform.IPhonePlayer)
			{
				camera.backgroundColor = Color.green;
			}
			//UNITY EDITOR IS YELLOW
			else if (Application.platform == RuntimePlatform.WindowsEditor)
			{
				camera.backgroundColor = Color.yellow;
			}

		}

		private void Restart()
		{
			Scene scene = SceneManager.GetActiveScene();
			//clear the console log
			ClearLog();
			SceneManager.LoadScene(scene.name);
		}

		public void ClearLog()
		{
			var assembly = Assembly.GetAssembly(typeof(UnityEditor.Editor));
			var type = assembly.GetType("UnityEditor.LogEntries");
			var method = type.GetMethod("Clear");
			method.Invoke(new object(), null);
		}

	}
}