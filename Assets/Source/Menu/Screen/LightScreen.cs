﻿namespace Developer.Screen
{
    using UnityEngine;
	using UnityEngine.Networking;
	using System.Threading;

	/// <inheritdoc />
	/// <summary>
	/// Controller for the screen that displays a <see cref="Developer.Component.TrafficLight" />.
	/// </summary>
	public sealed class LightScreen : Core.ScreenAbstract
	{
		//private
		private Developer.Component.TrafficLight lights = null;
		private Reporter reporter;
		private float timer = 0.01f;
		private float timeLeft;
		private int logTestCount = 100;
		private int currentLogTestCount;
		private bool finished, scriptAdded;

		private void Awake()
		{
			timeLeft = timer;
			GameObject trafficLightPrefab = Resources.Load<GameObject>("Prefabs/Component/TrafficLight/TrafficLight");
			GameObject trafficLightInstance = Instantiate(trafficLightPrefab, transform.Find("TrafficLightHolder"));
			lights = trafficLightInstance.AddComponent<Developer.Component.TrafficLight>();
			lights.SetState();
		}

		//TODO: According the rules, turn the lights on and off...
		//Hint: The networking functionality is inside the "Test" namespace.


		//Test Script

		void Update()
		{
			int drawn = 0;
			timeLeft -= Time.deltaTime;
			while (currentLogTestCount < logTestCount && drawn < 10 && timeLeft < 0)
			{
				Debug.Log("Test Log " + currentLogTestCount);
				Debug.LogError("Test LogError " + currentLogTestCount);
				Debug.LogWarning("Test LogWarning " + currentLogTestCount);

				if (currentLogTestCount % 3 == 0 && currentLogTestCount % 5 == 0)
				{
					lights.SetState(Test.LightColor.Green);
				}
				else if (currentLogTestCount % 3 == 0)
				{
					lights.SetState(Test.LightColor.Red);
				}
				else if (currentLogTestCount % 5 == 0)
				{
					lights.SetState(Test.LightColor.Yellow);
				}

				drawn++;
				currentLogTestCount++;
				//reset timer
				timeLeft = timer;

				if(currentLogTestCount == 100)
				{
					finished = true;
				}
			}

			if (finished)
			{
				//turn off lights 
				lights.SetState();
				//run another bool check to make sure finale screen script is added only once
				if (!scriptAdded)
				{
					Debug.Log("Finished");
					//add script to parent
					GameObject parent = gameObject.transform.parent.gameObject;
					parent.AddComponent<FinaleScreen>();
					gameObject.SetActive(false);
					scriptAdded = true;
				}

			}
		}
	}
}